#include <SPI.h>

#define SR_LATCH 10 //Pin for Latch on 74HC595
#define BUTTON A3
// Analog Value if Button pressed
// Resistor Values are:
// 47k from the Buttons down,
// 3k3 to GND,
// 3k3 from B0 to B1, 8k25 from B1 to B2,
// 15k from B2 to B3
// See picture for LTSpice Simulation in folder!
#define BTN_xxx0  18 
#define BTN_xx1x  33
#define BTN_xx10  51
#define BTN_x2xx  64
#define BTN_x2x0  83
#define BTN_x21x  98
#define BTN_x210 116
#define BTN_3xxx 127
#define BTN_3xx0 145
#define BTN_3x1x 160
#define BTN_3x10 179
#define BTN_32xx 192
#define BTN_32x0 210
#define BTN_321x 225
#define BTN_3210 243
#define BTN_WINDOW 5 //how much the ADC Value can differ

#define BUZZER 3
#define COMPARATOR 7
#define LED_ARDUINO 13
#define OPAMP A0

uint16_t data;
uint8_t dp;

// PROGMEM doesn't work!

typedef struct digits {
  // Anodes are: QA (Bit 0) - first digit, QB - fourth digit, QC - third digit, QD - second digit
  //                    1       2       4       8      10      20       40      80 (HEX)
  //                    0       1       2       3       4       5        6       7
  // Cathodes are: QA - a, QB - b, QC - f, QD - g, QE - c, QF - dp, QG - d, QH - e
  // 0 - a, b, c, d, e, f    - 0xd7
  // 1 - b, c                - 0x12
  // 2 - a, b, g, e, d       - 0xcb
  // 3 - a, b, c, d, g       - 0x5b
  // 4 - f, b, g, c          - 0x1e
  // 5 - a, f, g, c, d       - 0x5d
  // 6 - a, f, e, d, c, g    - 0xdd
  // 7 - a, b, c             - 0x13
  // 8 - a, b, c, d, e, f, g - 0xdf
  // 9 - a, b, c, d, f, g    - 0x5f
  // a - e, f, a, b, c, g    - 0x9f
  // b - f, e, d, c, g       - 0xdc
  // c - a, d, e, f          - 0xc5
  // d - b, c, d, e, f, g    - 0xda
  // e - a, d, e, f, g       - 0xcd
  // f - a, e, f, g          - 0x8d
  //
  //     a
  //     -- 
  // f  / / b
  //    --
  // e / / c
  //   --  .
  //   d   DP
  // DP is 0x20 or 0xDF because Low active
  uint16_t val; //         0x28   0xed   0x34   0xa4   0xe3   0xa2   0x22   0xec   0x20   0xa0   0x60   0x23   0x3a   0x25   0x32   0x72
  uint8_t cathodes[16] = {~0xd7, ~0x12, ~0xcb, ~0x5b, ~0x1e, ~0x5d, ~0xdd, ~0x13, ~0xdf, ~0x5f, ~0x9f, ~0xdc, ~0xc5, ~0xda, ~0xcd, ~0x8d};
  uint8_t anodes[4] = {1, 8, 4, 2};
  uint8_t c[4];
  uint8_t pos;
} DIGITS_t;
DIGITS_t digits;
#define DP 0xDF

void init_sevenseg() {
  SPI.begin();
  SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE2));
  pinMode(SR_LATCH, OUTPUT);
}

void send_digits(void){
  digitalWrite(SR_LATCH, LOW);
  SPI.transfer((digits.anodes[digits.pos]));
  SPI.transfer(digits.c[digits.pos]);
  digits.pos++;
  if (digits.pos > 3){
    digits.pos = 0;
    //digitalWrite(ledPin, digitalRead(ledPin) ^ 1);
  }
  digitalWrite(SR_LATCH, HIGH);
}

void update_digits(uint16_t val, uint8_t dp){
  digits.val = val; // get the actual cathode values for each position from progmem
  digits.c[3] = (digits.cathodes[(uint8_t)(digits.val & 0x000f)]);
  digits.c[2] = (digits.cathodes[(uint8_t)((digits.val & 0x00f0) >> 4)]);
  digits.c[1] = (digits.cathodes[(uint8_t)((digits.val & 0x0f00) >> 8)]);
  digits.c[0] = (digits.cathodes[(uint8_t)((digits.val & 0xf000) >> 12)]);
  if (dp & 1){
    digits.c[3] &= DP;
  }
  if (dp & 2){
    digits.c[2] &= DP;
  }
  if (dp & 4){
    digits.c[1] &= DP;
  }
  if (dp & 8){
    digits.c[0] &= DP;
  }
}

uint8_t isApproxEqual(uint8_t input, uint8_t border){
  return ((input > border - BTN_WINDOW) && (input < border + BTN_WINDOW))?1:0;
}

uint8_t GetButton(uint8_t v){ //Return Value is one Bit per button
  uint8_t ret;
  ret = 0;
  //v >>=2;
  if (isApproxEqual(v, BTN_xxx0)){
    ret |= 1; 
  }
  else if (isApproxEqual(v, BTN_xx1x)){
    ret |= 2; 
  }
  else if (isApproxEqual(v, BTN_xx10)){
    ret |= 3; 
  }      
  else if (isApproxEqual(v, BTN_x2xx)){
    ret |= 4; 
  }
  else if (isApproxEqual(v, BTN_x2x0)){
    ret |= 5; 
  }      
  else if (isApproxEqual(v, BTN_x21x)){
    ret |= 6; 
  }      
  else if (isApproxEqual(v, BTN_x210)){
    ret |= 7; 
  }      
  else if (isApproxEqual(v, BTN_3xxx)){
    ret |= 8; 
  }      
  else if (isApproxEqual(v, BTN_3xx0)){
    ret |= 9; 
  }      
  else if (isApproxEqual(v, BTN_3x1x)){
    ret |= 0x0A; 
  }      
  else if (isApproxEqual(v, BTN_3x10)){
    ret |= 0x0B; 
  }      
  else if (isApproxEqual(v, BTN_32xx)){
    ret |= 0x0C; 
  }      
  else if (isApproxEqual(v, BTN_32x0)){
    ret |= 0x0D; 
  }      
  else if (isApproxEqual(v, BTN_321x)){
    ret |= 0x0E; 
  }      
  else if (isApproxEqual(v, BTN_3210)){
    ret |= 0x0F; 
  }      
  return ret;
}

//timer compare interrupt service routine
ISR(TIMER1_COMPA_vect){
  send_digits();
}

void setup() {
  // initialize timer1 
  // TIMER 1 for interrupt frequency 200 Hz:
  cli(); // stop interrupts
  TCCR1A = 0; // set entire TCCR1A register to 0
  TCCR1B = 0; // same for TCCR1B
  TCNT1  = 0; // initialize counter value to 0
  // set compare match register for 200 Hz increments
  OCR1A = 9999; // = 16000000 / (8 * 200) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12, CS11 and CS10 bits for 8 prescaler
  TCCR1B |= (0 << CS12) | (1 << CS11) | (0 << CS10);
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);

  // Timer 2 for Buzzer
  pinMode(BUZZER,OUTPUT);
  TCCR2A = 0;
  TCCR2B = _BV(CS22); // CLK/64 Presc.
  OCR2A = 0x80;
  sei(); // allow interrupts
  init_sevenseg();
  Serial.begin(9600);
  pinMode(COMPARATOR, INPUT);
}

void beep( uint8_t on){
  if (on){
    TCCR2A = _BV(WGM21) | _BV(COM2B0); //CTC Mode, Toggle OC2B on compare match
  }
  else{
    TCCR2A = 0;
  }
}
void loop() {
  uint8_t inByte, btn;
  uint8_t Buttonval, opamp;
  if (Serial.available()) {
    inByte = Serial.read();
    Serial.write(inByte);
  }
  Buttonval = analogRead(BUTTON) >> 2;
  btn = GetButton(Buttonval);
  opamp = analogRead(OPAMP);

  Serial.print("Button = ");
  Serial.print(Buttonval);
  Serial.print("\t output = ");
  Serial.print(btn);
  Serial.print(" AD0 = ");
  Serial.println(opamp);
  if (btn){
    update_digits(btn, dp);
    beep(1);
  }
  else{
    update_digits(data++, dp);
    beep(0);
  }
  digitalRead(COMPARATOR)?dp=1:dp=2;

  delay(500);
}
  