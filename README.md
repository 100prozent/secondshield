# README #

### What is this repository for? ###

* This Repository is for the PCB-Design Files, the Software and the Documentation for the second shield.
* Second shield is an Arduino Extension Board. Board Size is about the same as an Arduino Uno.

### Version History: ###
* 0.7: Values changed, Code added, LTSpice added
* 0.6: Buzzer directly on PWM
* 0.4: Buzzer opamp changed to non-inverting, Board finished
* 0.3: Initial Version on bitbucket.